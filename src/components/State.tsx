import { useState } from "react";

export default function Main() {

  const [nr, setNr] = useState(0);

  // fungerar ej - eftersom att den inte ändras/updateras efter initial rendering av sidan
  let number = 3;

  const increase = () => {
    console.log(nr);
    setNr(nr + 1);
  };

  return (
    <>
      <div className="container">
        <p>{nr}</p>
        <button onClick={increase}>Knapp</button>
      </div>
    </>
  );
}


function Toggler() {
    const [showMessage, setShowMessage] = useState(false);

    const handleClick = () => {
        setShowMessage(!showMessage)
    }
  
    return (
      <div>
        <button onClick={handleClick}>
          {showMessage ? 'Göm' : 'Visa'}
        </button>
        {showMessage && <p>Nu syns jag!</p>}
      </div>
    );
  };
  
  
  function Example() {


    const [text, setText] = useState("");

    function handleChange(e:React.ChangeEvent<HTMLInputElement>){
        setText(e.target.value)
    }
  
    return (
      <div>

        <input
          type="text"
          value={text}
          onChange={e => handleChange(e)}
        />
        <p>{text}</p>
      </div>
    );
  }
  
