class Book {
    constructor(
      public id: number,
      public title: string,
      public author: string,
      public liked: boolean
    ) {
      this.id = id;
      this.title = title;
      this.author = author;
      this.liked = liked;
    }
  }
  
  export interface IMainView {
    id: number;
    title: string;
    author: string;
    liked: boolean;
  }
  
  
  
  export const fetchedData = [
    new Book(1, "Winter", "Ali Smith", false),
    new Book(2, "Ulyssus", "James Joyce", false),
    new Book(3, "Frankenstein", "Mary Shelly", false),
    new Book(4, "Lövengripska ringen", "Selma Lagerlöf", false),
  ];