import React, { useContext, useState } from "react";
import { MainContext } from "../MainViewContext";

export default function BooksComponent() {
  return (
    <div>
      <p>Snygg bild</p>

      {/* massa html och material UI taggar */}
      <BookList></BookList>

      <p>Andra komponenter med information</p>
    </div>
  );
}

function BookList() {
  const { books, setBooks } = useContext(MainContext);


  const handleClick = () => {
// setBooks()
}



  return (
    <>
      <h2>Här är min lista av top 5 böcker:</h2>
      {books.map((b) => (
        <ListItem key={b.id} title={b.title} author={b.author} handleClick={handleClick}></ListItem>
      ))}
    </>
  );
}

interface IListItem {
  title: string;
  author: string;
  handleClick: () => void
}

function ListItem({ title, author, handleClick }: IListItem) {
  return (
    <>
    <li>
      <span onClick={handleClick}>{title} skriven av {author}</span>
    </li>
    </>
  );
}
