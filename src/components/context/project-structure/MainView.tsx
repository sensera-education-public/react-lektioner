import React from "react";
import BooksComponent from "./components/BooksComponent";
import MainViewContext from "./MainViewContext";

export default function MainView() {
  return (
    <MainViewContext>
      <BooksComponent />
    </MainViewContext>
  );
}
