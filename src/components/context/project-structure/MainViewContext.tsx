import React, { createContext, useState } from "react";
import { fetchedData, IMainView } from "./MainViewModel";

interface MainViewProps {
  children: React.ReactNode;
}

interface IMainContext {
  books: IMainView[],
  setBooks: React.Dispatch<React.SetStateAction<IMainView[]>>
}

export const MainContext = createContext<IMainContext>({books: [], setBooks: () => {}});

export default function MainViewContext({ children }: MainViewProps) {
  const [books, setBooks] = useState<IMainView[]>(fetchedData);

  return (
    <MainContext.Provider value={{ books, setBooks }}>
      {children}
    </MainContext.Provider>
  );
}
