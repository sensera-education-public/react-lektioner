import { createContext, useState } from "react";
import Child from "./Child";

interface IUserContext {
  user: string;
  setUser: React.Dispatch<React.SetStateAction<string>>
}

export const UserContext = createContext<IUserContext>({ user: "" , setUser: () => {}});

export default function Parent() {
  const [user, setUser] = useState("Johanna");

  return (
    <>
      <UserContext.Provider value={{user, setUser}}>
        <Child></Child>
      </UserContext.Provider>
    </>
  );
}
