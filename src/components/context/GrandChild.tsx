import { useContext } from "react";
import { UserContext } from "./Parent";

export default function GrandChild() {

const {user, setUser} = useContext(UserContext)


  return (<>Hej {user}</>);
}
