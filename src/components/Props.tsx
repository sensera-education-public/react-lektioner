class User {
    constructor(
      public userId: number,
      public userName: string,
      public books: string[]
    ) {
      this.userId = userId;
      this.userName = userName;
      this.books = books;
    }
  }
  
  interface IUser {
    userName: string;
    books: string[];
  }
  


interface IBooks {
    books: string[];
  }
  
  const Books = (props: IBooks) => {
    return (
      <>
        {props.books.map((book) => (
          <li>{book}</li>
        ))}
      </>
    );
  };
  
  const Display = (props: IUser) => {
    return (
      <>
        <p>{props.books}</p>
        <Books books={props.books}></Books>
      </>
    );
  };
  
  export default function Main() {
  
      const user = new User(3, "Hanna", ["Winter", "Summer", "Autumn"])
  
    return (
      <>
        <Display
          userName={user.userName}
          books={user.books}
        ></Display>
      </>
    );}