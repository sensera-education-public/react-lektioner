import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import About from './About'
import Header from './Header'
import Login from './Login'
import Main from './Main'

export default function Root() {
  return (
    <BrowserRouter>
    <Header></Header>
    <Routes>
      <Route path="/" element={<Main />} />
      <Route path="/about/:user" element={<About />} />
      <Route path="/login" element={<Login />} />
      <Route path="*" element={<Main />}/>
    </Routes>
    </BrowserRouter>
  )
}
