import React from 'react'
import { useParams } from 'react-router-dom'

export default function About() {

    const {user} = useParams()

  return (
    <div>Hej {user}</div>
  )
}
