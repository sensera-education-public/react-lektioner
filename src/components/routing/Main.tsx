import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

export default function Main() {
    const [user, setUser] = useState(4)

    const navigate = useNavigate()

    const handleClick = () =>{
        //navigera och skicka med användarnamn
    navigate(`/about/${user}`)
    }
  return (
    <div>
        <button onClick={handleClick}>Knapp</button>
    </div>
  )
}
