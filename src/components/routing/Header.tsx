import React from "react";
import { Link } from "react-router-dom";

export default function Header() {
  return (
   
    <div className="container">

      <Link to="/about/">About</Link>
      <Link to="/main">Main</Link>
      <Link to="/login">Login</Link>
    </div>
    
  );
}
