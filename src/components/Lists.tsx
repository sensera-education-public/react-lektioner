import { useState } from "react";

const listOfBooks = [
  { id: 1, title: "Winter", author: "Ali Smith", year: 2019, read: false },
  { id: 2, title: "Ulyssus", author: "James Joyce", year: 2020, read: false },
  {
    id: 3,
    title: "Frankenstein",
    author: "Mary Shelly",
    year: 2021,
    read: false,
  },
  {
    id: 4,
    title: "Lövengripska ringen",
    author: "Selma Lagerlöf",
    year: 2022,
    read: false,
  },
];

export default function List() {
  const [bookList, setBookList] = useState(listOfBooks);

  const makeRead = (id: number) => {
    //veta vilken bok som blivit klickad på
    const updatedBookList = bookList.map((book) => {
      if (id === book.id) {
        //ändra fält i objektet
        return { ...book, read: !book.read };
      }
      return book;
    });
    
    //ändra på state-variablens värde
    setBookList(updatedBookList);
    console.log(updatedBookList);
  };

  const books = bookList.map((book) => (
    <Book
      key={book.id}
      title={book.title}
      author={book.author}
      year={book.year}
      id={book.id}
      makeRead={makeRead}
    ></Book>
  ));

  return <>{books}</>;
}



interface IBook {
    title: string;
    author: string;
    id: number;
    year: number;
    makeRead(id: number) : void
  }
  
  function Book({makeRead, title, author, year, id}: IBook) {
    return (
      <>
        <li onClick={() => makeRead(id)}>
          {title} skriven av {author} utgiven år: {year}
        </li>
      </>
    );
  };