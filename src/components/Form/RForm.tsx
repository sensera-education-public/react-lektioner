import { useForm } from "react-hook-form";

interface IFormData {
  name: string;
  email: string;
  gender: string;
}

const RForm = () => {
  const {register, handleSubmit, formState: { errors } } = useForm<IFormData>()
  const onSubmit = (data :IFormData) => console.log(data);

return (<>
 <form onSubmit={handleSubmit(onSubmit)}>
    <div className="container">
      <label htmlFor="username">Namn:</label>
      <input
        id="username"
        type="text"
      {...register("name", { required: true })}
      />
      {errors.name && <span>This field is required</span>}
      <label htmlFor="email">E-mail:</label>
      <input
        id="email"
        type="email"
       {...register("email")}
      />
      <label htmlFor="gender">Kön:</label>
      <select
        id="gender"
       {...register("gender")}
      >
        <option value="">Välj:</option>
        <option value="nonbinary">Hen</option>
        <option value="female">Kvinna</option>
        <option value="male">Man</option>
      </select>
      <button type="submit">Knapp</button>
    </div>
  </form>
    </>
  );
};

export default RForm;
