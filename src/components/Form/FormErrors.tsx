import { useState } from "react";

interface FormData {
  username: string;
  email: string;
  gender: string;
}
function FormError(){
  const [formValues, setFormValues] = useState<FormData>({
    username: "",
    email: "",
    gender: "",
  });

  const [formErrors, setFormErrors] = useState<Partial<FormData>>({
    username: "",
    email: "",
    gender: "",
  });

  const validate = () => {
    const errors: Partial<FormData> = {};
    if (!formValues.username) {
      errors.username = "Name is required";
    }
    if (!formValues.email) {
      errors.email = "Email is required";
    } else if (!/\S+@\S+\.\S+/.test(formValues.email)) {
      errors.email = "Email is invalid";
    }
    if (!formValues.gender) {
      errors.gender = "Gender is required";
    }
    setFormErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const handleChange = ( event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    setFormValues({
      ...formValues,
      [event.target.name]: event.target.value,
    });
    setFormErrors({
      ...formErrors,
      [event.target.name]: event.target.value
        ? ""
        : `${event.target.name} is required`,
    });
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (validate()) {
      console.log(formValues);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="username">Name:</label>
          <input
            type="text"
            id="name"
            name="username"
            value={formValues.username}
            onChange={handleChange}
          />
          {formErrors.username && <span>{formErrors.username}</span>}
        </div>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formValues.email}
            onChange={handleChange}
          />
          {formErrors.email && <span>{formErrors.email}</span>}
        </div>
        <div>
          <label htmlFor="gender">Gender:</label>
          <select
            id="gender"
            name="gender"
            value={formValues.gender}
            onChange={handleChange}
          >
            <option value="">Select gender</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="nonbinary">Non-binary</option>
          </select>
          {formErrors.gender && <span>{formErrors.gender}</span>}
        </div>
        <button type="submit">Knapp</button>
      </form>
    </>
  );
};
export default FormError;
