import { useState } from "react";
import FormError from "./FormErrors";

interface FormData {
  username: string;
  email: string;
  gender: string;
}

function Form(){
  const [formValues, setFormValues] = useState<FormData>({
    username: "",
    email: "",
    gender: "",
  });

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    console.log(formValues);
  };

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    let {name, value} = event.target
    setFormValues({
      ...formValues,
      [name]: value
    });
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="username">Name:</label>
          <input
            type="text"
            id="name"
            name="username"
            value={formValues.username}
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formValues.email}
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="gender">Gender:</label>
          <select
            id="gender"
            name="gender"
            value={formValues.gender}
            onChange={handleChange}
          >
            <option value="">Select gender</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="nonbinary">Non-binary</option>
          </select>
        </div>
        <button type="submit">Submit</button>
      </form>
      <FormError></FormError>
    </>
  );
};

export default Form;
