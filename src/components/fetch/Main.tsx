/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import {
  QueryClient,
  QueryClientProvider,
} from "react-query";
import Posts from "./Display";

const queryClient = new QueryClient();

export default function Main() {

  return (
    <QueryClientProvider client={queryClient}>
        <Posts/>
    </QueryClientProvider>
  );
}
