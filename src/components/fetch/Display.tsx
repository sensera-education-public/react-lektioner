import axios from "axios";
import { useQuery, useQueryClient } from "react-query";

interface Post{
    id : number,
    title: string,
    body: string
}

function usePosts() {
  return useQuery("posts", async () => {
    const { data } = await axios.get<Post[]>(
      "https://jsonplaceholder.typicode.com/posts"
    );
    return data;
  });
}

export default function Posts() {

  const { status, data, error, isFetching } = usePosts();

  return (
    <div>
      <h1>Posts</h1>
      <div>
        {status === "loading" ? (
          "Loading..."
        ) : status === "error" ? (
          <span>Error:</span>
        ) : (
          <>
            <div>
              {data?.map((post) => (
                <p key={post.id}>{post.title}</p>
              ))}
            </div>
            <div>{isFetching ? "Background Updating..." : " "}</div>
          </>
        )}
      </div>
    </div>
  );
}
