import React from 'react';
import logo from './logo.svg';
import './App.css';
import Parent from './components/context/Parent';
import MainView from './components/context/project-structure/MainView';
import Main from './components/fetch/Main';

function App() {
  return (
    <div className="App">
<Main></Main>    </div>
  );
}

export default App;
